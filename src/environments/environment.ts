// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC0GjAz_9ydgpsUirDogDnCxcTJXZ9yH0U",
    authDomain: "confianzav-result-dist-backend.firebaseapp.com",
    databaseURL: "https://confianzav-result-dist-backend.firebaseio.com",
    projectId: "confianzav-result-dist-backend",
    storageBucket: "confianzav-result-dist-backend.appspot.com",
    messagingSenderId: "643220325200"
  },
  baseURL:'http://localhost:9000/api/v1',
  resultDisplayTime: 57600000
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
