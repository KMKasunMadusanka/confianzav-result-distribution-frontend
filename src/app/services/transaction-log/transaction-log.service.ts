import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class TransactionLogService {

  constructor(private _HttpClient:HttpClient) { }

  getCndidateTransactionLog (id:Number) {
    return this._HttpClient.get <any> (environment.baseURL+'/election/candidate/'+id);
  }


}
