import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  private logedStatus = new BehaviorSubject<boolean>(false);
  logedStatusCast = this.logedStatus.asObservable();

  private electionID = new BehaviorSubject<number>(1);
  electionIdCast = this.electionID.asObservable();

  private usertype = new BehaviorSubject<string>('user type');
  usertypeCast = this.usertype.asObservable();

  onChangeloginStatus(newloginStatus) {
    this.logedStatus.next(newloginStatus);
  }

  onChangeElectionId(newId) {
    this.electionID.next(newId);
  }

  onChangeUsertype(newUsertype) {
    this.usertype.next(newUsertype);
  }

  constructor(private _Router: Router,
    private http: HttpClient) { }

  sendToken(token: string) {
    localStorage.setItem("LoggedInUser", token)
    this.onChangeloginStatus(true);
  }
  getToken() {
    return localStorage.getItem("LoggedInUser")
  }
  isLoggednIn() {
    return this.getToken() !== null;
  }
  logout() {
    localStorage.removeItem("LoggedInUser");
    this._Router.navigate(["login"]);
    this.onChangeloginStatus(false);
  }

  startSync (id) {
    let url = 'http://localhost:9000/api/v1/digitalsignature/sinature/genarate/start?id='+id
    return this.http.get(url);
  }
}
