import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs'
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AuthServiceService } from '../../services/Auth/auth-service.service'
@Injectable({
  providedIn: 'root'
})
export class VoteServiceService {

  private filteredPalathsabaArray = new BehaviorSubject<any>([]);
  palathsabaArrayCast = this.filteredPalathsabaArray.asObservable();

  onChangePalathsabaArray(newPlathsabaArray) {
    this.filteredPalathsabaArray.next(newPlathsabaArray);
  }

  //firebase ref
  electionCollection: AngularFirestoreCollection<any> = this.afs.collection('Election');
  candidateCollection: AngularFirestoreCollection<any> = this.afs.collection('Candidate');
  voteCollection: AngularFirestoreCollection<any> = this.afs.collection('Vote');

  electionData: any = [];
  candidateData: any = [];
  voteData: any = [];

  constructor(private afs: AngularFirestore,
    private _AuthServiceService: AuthServiceService
  ) { }

  manipulateVoteData = Observable.create((observer) => {

    let finalCnadidateList = [];
    //value change observables
    // let electionObs = this.electionCollection.valueChanges();
    // let candidateObs = this.candidateCollection.valueChanges();
    // let voteObs = this.voteCollection.valueChanges();

    this._AuthServiceService.electionIdCast.subscribe(data => {

      this.candidateCollection.valueChanges().subscribe((candidate_val) => {
        this.candidateData =[];
        for (let candidateObj of candidate_val){
          if (candidateObj.election_id == data){
            this.candidateData.push(candidateObj);
          }
        }
  
        this.voteCollection.valueChanges().subscribe((vote_val) => {
          this,this.voteData =[];
          for (let voteObj of vote_val){
            if (voteObj.election_id == data){
              this.voteData.push(voteObj);
            }
          }
         
          let candidateArray = [];
          let candidateVoteDetailsArray = [];
          let selectedCandidateArray = [];
          candidateArray = this.genarateCandaiateWiseResult(this.voteData, this.candidateData);
          this.palathsabaArrayCast.subscribe((selectedPlathsaba) => {
            finalCnadidateList = [];

            for (const genratedCandaidateObj of candidateArray) {
              let vote_counting_palathsaba = [];
              let candidate_eligibility_flag = false;
              const candidatePalathsabaArray = genratedCandaidateObj.palathsaba_id_list;
              let candidate_vote_count = 0;
              //genarate palathsaba array which is similer to candidate palathsabas
              for (const palathsabaid of candidatePalathsabaArray) {
                for (const selectedPlathsabaObj of selectedPlathsaba) {
                  if (palathsabaid == selectedPlathsabaObj.id) {
                    candidate_eligibility_flag = true;
                    let temObj = {
                      "id": palathsabaid,
                      "eligible_vote": selectedPlathsabaObj.elegible_vote_count
                    }
                    vote_counting_palathsaba.push(temObj);
                  }
                }
              }

              //count vote for each candidate
              for (const voteObj of this.voteData) {
                //check if voteid of vote array is similler with selected palathsaba ids
                for (const vote_count_palathsaba_id_obj of vote_counting_palathsaba) {
                  if (vote_count_palathsaba_id_obj.id == voteObj.id && genratedCandaidateObj.candidate_id == voteObj.candidate_id) {
                    candidate_vote_count = candidate_vote_count + voteObj.vote_count;
                  }
                }
              }

              //update vote count of candidate
              genratedCandaidateObj.vote_count = candidate_vote_count;
              genratedCandaidateObj.elegible_vote_count_obj = vote_counting_palathsaba
              //put correct candate objects into array.
              if (candidate_eligibility_flag) {
                finalCnadidateList.push(genratedCandaidateObj);
              }
            }
            observer.next(finalCnadidateList);
          });
        });
      });
    });
  });

  genarateCandaiateWiseResult(voteArray, candidateArray) {
    let updatedVoteArray = [];
    let candidate_palathsaba_Array = [];
    for (const candidateObj of candidateArray) {
      for (const voteObj of voteArray) {
        if (voteObj.candidate_id == candidateObj.candidate_id) {
          candidate_palathsaba_Array.push(voteObj.id);
        }

      }
      let tempObj = {
        "election_id": candidateObj.election_id,
        "palathsaba_id_list": candidate_palathsaba_Array,
        "candidate_name": candidateObj.candidate_name,
        "candidate_id": candidateObj.candidate_id,
        "is_active": candidateObj.isActive,
        "party_name": candidateObj.party,
        "party_color": candidateObj.party_color,
        "party_icon": candidateObj.party_icon,
        "vote_count": 0,
        "candidate_icon": candidateObj.image
      }
      updatedVoteArray.push(tempObj);
      candidate_palathsaba_Array = [];
    }
    return updatedVoteArray;
  }

}
