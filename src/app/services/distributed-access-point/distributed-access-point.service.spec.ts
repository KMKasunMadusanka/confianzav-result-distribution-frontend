import { TestBed, inject } from '@angular/core/testing';

import { DistributedAccessPointService } from './distributed-access-point.service';

describe('DistributedAccessPointService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DistributedAccessPointService]
    });
  });

  it('should be created', inject([DistributedAccessPointService], (service: DistributedAccessPointService) => {
    expect(service).toBeTruthy();
  }));
});
