import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs'
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AuthServiceService } from '../../services/Auth/auth-service.service'
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class DistributedAccessPointService {

  //firebase ref
  electionCollection: AngularFirestoreCollection<any> = this.afs.collection('Election');
  candidateCollection: AngularFirestoreCollection<any> = this.afs.collection('Candidate');
  voteCollection: AngularFirestoreCollection<any> = this.afs.collection('Vote');

  electionData: any;
  candidateData: any;
  voteData: any;



  constructor(private _HttpClient: HttpClient,
    private afs: AngularFirestore,
    private _AuthServiceService: AuthServiceService) { }

  httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type':  'application/xml', //<- To SEND XML
      'Accept': 'application/xml',       //<- To ask for XML
      'Response-Type': 'text'             //<- b/c Angular understands text
    })
  };

  getXML = Observable.create((observer) => {

    this.electionCollection.valueChanges().subscribe((election_val) => {

      this._AuthServiceService.electionIdCast.subscribe(data => {
        for (let electionobj of election_val) {
          if (electionobj.id == data) {
            this.candidateCollection.valueChanges().subscribe((candidate_val) => {
              let canidateArray = [];
              for (let canidateObj of candidate_val) {
                if (canidateObj.election_id == data) {
                  canidateArray.push(canidateObj);
                }
              }
              this.voteCollection.valueChanges().subscribe((vote_val) => {
                let voteArray = [];
                for (let voteObj of vote_val) {
                  if (voteObj.election_id == data) {
                    voteArray.push(voteObj);
                  }
                }
                let finalObj =
                {
                  content: {
                    'Election': electionobj,
                    'Candidate': canidateArray,
                    'Vote': voteArray
                  }
                }

                console.log(finalObj)
                this._HttpClient.post(environment.baseURL+'/distributedpoint/elections/xml', finalObj, this.httpOptions)
                  .subscribe((data) => {
                    //console.log(data);
                    observer.next(data);
                  })
              })
            })
          }
        }
      });
    })

  })

}
