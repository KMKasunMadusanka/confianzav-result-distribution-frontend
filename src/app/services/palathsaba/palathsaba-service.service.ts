import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VoteServiceService } from '../election-result/vote-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PalathsabaServiceService {

  constructor(private http: HttpClient, private _VoteServiceService:VoteServiceService) { }
  public provinceContainer: any;
  public distrcitcontainer: any;
  public palathsabaContainer: any;

  private selectedProvince: string;
  private selectedDistrict: string;
  private selectedPalathsaba: string;

  private provincialElectionFalg : boolean = false;

  public setSelectedProvince(val) {
    this.selectedProvince = val;
  }
  public setSelectedDistrict(val) {
    this.selectedDistrict = val;
  }
  public setSelectedPalathsaba(val) {
    this.selectedPalathsaba = val;
  }

  public setProvincialElectionFlag (val) {
    this.provincialElectionFalg = val;
  }

  public getProvincialElectionFlag(){
    return this.provincialElectionFalg;
  }

  getAllPalathsaba = () => {
    return this.http.get('/assets/data/palathsaba.json');
  }


  filterData = Observable.create((observer) => {
    let selectedDataArray=[];
    observer.next(this.selectedProvince);
    this.getAllPalathsaba().subscribe((data: any) => {

     // console.log("province :"+this.selectedProvince+"  district :"+this.selectedDistrict+"  palathsaba :"+this.selectedPalathsaba);

      if (this.selectedProvince == 'all' && this.selectedDistrict == 'all' && this.selectedPalathsaba == 'all') {
        selectedDataArray = data;
        this.setProvincialElectionFlag(false);
      }
      else if (this.selectedProvince != 'all' && this.selectedDistrict == 'all' && this.selectedPalathsaba == 'all') {
        for (const palathdabaObj of data) {
          if (palathdabaObj.province === this.selectedProvince) {
            selectedDataArray.push(palathdabaObj);
          }
        }
        this.setProvincialElectionFlag(true);
      }
      else if (this.selectedProvince == 'all' && this.selectedDistrict != 'all' && this.selectedPalathsaba == 'all') {
        for (const palathdabaObj of data) {
          if (palathdabaObj.district === this.selectedDistrict) {
            selectedDataArray.push(palathdabaObj);
          }
        }
        this.setProvincialElectionFlag(true);
      }
      else if (this.selectedProvince == 'all' && this.selectedDistrict == 'all' && this.selectedPalathsaba != 'all') {
        for (const palathdabaObj of data) {
          if (palathdabaObj.palathsaba === this.selectedPalathsaba) {
            selectedDataArray.push(palathdabaObj);
          }
        }
        this.setProvincialElectionFlag(true);
      }
      else if (this.selectedProvince != 'all' && this.selectedDistrict != 'all' && this.selectedPalathsaba == 'all') {
        for (const palathdabaObj of data) {
          if (palathdabaObj.province === this.selectedProvince && palathdabaObj.district === this.selectedDistrict) {
            selectedDataArray.push(palathdabaObj);
          }
        }
        this.setProvincialElectionFlag(true);
      }
      else if (this.selectedProvince == 'all' && this.selectedDistrict != 'all' && this.selectedPalathsaba != 'all') {
        for (const palathdabaObj of data) {
          if (palathdabaObj.district === this.selectedDistrict && palathdabaObj.palathsaba === this.selectedPalathsaba) {
            selectedDataArray.push(palathdabaObj);
          }
        }
        this.setProvincialElectionFlag(true);
      }
      else if (this.selectedProvince != 'all' && this.selectedDistrict == 'all' && this.selectedPalathsaba != 'all') {
        for (const palathdabaObj of data) {
          if (palathdabaObj.province === this.selectedProvince && palathdabaObj.palathsaba === this.selectedPalathsaba) {
            selectedDataArray.push(palathdabaObj);
          }
        }
        this.setProvincialElectionFlag(true);
      }
      else if (this.selectedProvince != 'all' && this.selectedDistrict != 'all' && this.selectedPalathsaba != 'all') {
        for (const palathdabaObj of data) {
          if (palathdabaObj.province === this.selectedProvince && palathdabaObj.district === this.selectedDistrict && palathdabaObj.palathsaba === this.selectedPalathsaba) {
            selectedDataArray.push(palathdabaObj);
          }
        }
        this.setProvincialElectionFlag(true);
      }

      let provinceArray = [];
      let districtArray = [];
      let palathsabaArray = [];
      let provinceStatus = true;
      let districtStatus = true;
      let palathsabaStatus = true;

      //console.log(selectedDataArray);

      for (const locationObj of selectedDataArray) {
       
        for (const provinceObj of provinceArray) {
          if (locationObj.province == provinceObj.viewValue) {
            provinceStatus = false;
          }
        }
        if (provinceStatus) {
          const obj = {
            'value': locationObj.province,
            'viewValue': locationObj.province,
            'id': locationObj.palathsaba_id
          };
          provinceArray.push(obj);
        }
        
        for (const districtObj of districtArray) {
          if (locationObj.district == districtObj.viewValue) {
            districtStatus = false;
          }
        }
        if (districtStatus) {
          const obj = {
            'value': locationObj.district,
            'viewValue': locationObj.district,
            'id': locationObj.palathsaba_id
          };
          districtArray.push(obj);
        }
       
        for (const palathsabaObj of palathsabaArray) {
          if (locationObj.palathsaba == palathsabaObj.viewValue) {
            palathsabaStatus = false;
          }
        }
        if (palathsabaStatus) {
          const obj = {
            'value': locationObj.palathsaba,
            'viewValue': locationObj.palathsaba,
            'id': locationObj.palathsaba_id,
            'elegible_vote_count':locationObj.elegible_votes
          };
          palathsabaArray.push(obj);
        }
        palathsabaStatus = true;
        districtStatus = true;
        provinceStatus = true;
      }
      
      const mainObj = {
        'province': provinceArray.sort(function (a, b) { return (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0); }),
        'district': districtArray.sort(function (a, b) { return (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0); }),
        'palathsaba':palathsabaArray.sort(function (a, b) { return (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0); })
      }
      this._VoteServiceService.onChangePalathsabaArray(palathsabaArray.sort(function (a, b) { return (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0); }));
      observer.next(mainObj);

    })
  });


}
