import { TestBed, inject } from '@angular/core/testing';

import { PalathsabaServiceService } from './palathsaba-service.service';

describe('PalathsabaServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PalathsabaServiceService]
    });
  });

  it('should be created', inject([PalathsabaServiceService], (service: PalathsabaServiceService) => {
    expect(service).toBeTruthy();
  }));
});
