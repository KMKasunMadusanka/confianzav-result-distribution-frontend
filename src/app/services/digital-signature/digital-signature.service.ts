import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class DigitalSignatureService {

   //firebase ref
   digitalSignature: AngularFirestoreCollection<any> = this.afs.collection('Digital_Signature');
 

  constructor(private afs: AngularFirestore) { }

  manupulateDigitalSignatureData = Observable.create((observer) => {
   
    this.digitalSignature.valueChanges().subscribe((digitalSignatureData)=>{
        observer.next(digitalSignatureData);
    });
  });
    

}
