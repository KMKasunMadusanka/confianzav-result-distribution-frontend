import { TestBed, inject } from '@angular/core/testing';

import { DigitalSignatureService } from './digital-signature.service';

describe('DigitalSignatureService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DigitalSignatureService]
    });
  });

  it('should be created', inject([DigitalSignatureService], (service: DigitalSignatureService) => {
    expect(service).toBeTruthy();
  }));
});
