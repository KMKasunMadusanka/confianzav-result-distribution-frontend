import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AuthServiceService } from '../../services/Auth/auth-service.service'

@Injectable({
  providedIn: 'root'
})
export class PrivateElectionResultService {

  candidateCollection: AngularFirestoreCollection<any> = this.afs.collection('Candidate');
  candidateFilteredData: any;

  constructor(private afs: AngularFirestore,
    private _AuthServiceService: AuthServiceService) { }

  manupulatePrivateVoteData = Observable.create((observer) => {

    this._AuthServiceService.electionIdCast.subscribe(data => {

      this.candidateCollection.valueChanges().subscribe((candidateData) => {
        this.candidateFilteredData = [];
        let finalizeCandidateArray = [];
        for (let candidateObj of candidateData) {
          if (candidateObj.election_id == data) {
            this.candidateFilteredData.push(candidateObj);
          }
        }

        for (const candidateobj of this.candidateFilteredData) {
          let temobj = {
            "candidate_id":candidateobj.candidate_id,
            "election_id": candidateobj.election_id,
            "candidate_name": candidateobj.candidate_name,
            "private_vote_Count": candidateobj.private_vote_Count,
            "isActive": candidateobj.isActive,
            "candidate_icon": candidateobj.image
          }
          finalizeCandidateArray.push(temobj);
        }
        observer.next(finalizeCandidateArray);
      });
    });
  });


}
