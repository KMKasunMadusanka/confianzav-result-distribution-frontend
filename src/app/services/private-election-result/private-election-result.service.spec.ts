import { TestBed, inject } from '@angular/core/testing';

import { PrivateElectionResultService } from './private-election-result.service';

describe('PrivateElectionResultService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrivateElectionResultService]
    });
  });

  it('should be created', inject([PrivateElectionResultService], (service: PrivateElectionResultService) => {
    expect(service).toBeTruthy();
  }));
});
