import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-about-us",
  templateUrl: "./about-us.component.html",
  styleUrls: ["./about-us.component.scss"]
})
export class AboutUsComponent implements OnInit {
  mute = true;
  benifits = [
    {
      title: "Immutable and anonymous",
      content:
        "ConfianzaV is based on blockchain technology, which makes voting 100% secure and immutable. Voter anonymity is guaranteed by transparent crypto algorithms.",
      icon: "/assets/images/about-us-page/immutable.svg"
    },
    {
      title: "Easy to organize, easy to vote",
      content:
        "With Confianza V, creating a vote is intuitive, easy and fast. No coding knowledge is required. Voting can be conducted on the go on a smartphone or tablet.",
      icon: "/assets/images/about-us-page/clock.svg"
    },
    {
      title: "Transparent and auditable",
      content:
        "One of the main characteristics of blockchain technology is its transparency. The crypto algorithms that we use on top of it are merely mathematics",
      icon: "/assets/images/about-us-page/transparent.svg"
    }
  ];

  result_intro = {
    title: " Live Result Update",
    content:
      " Voting data will be udate in the dashbord in live manner by default. It will depend on the configuratioon that has given in the election creation. Result displaying mechanism also varing according to the elction configuration given at the election creation state",
    video_path: "https://s3.amazonaws.com/it15116420awabucket/liveupdate.mp4"
  };

  addtional_features = [
    {
      title: "Distributed Access Point",
      video_path:
        "https://s3.amazonaws.com/it15116420awabucket/distributedAccessPoint.mp4",
      content:
        "Any one can acccess the voting data though RSS feeds. Its Specially for Medias"
    },
    {
      title: "Verify Election Reults Security",
      video_path: "https://s3.amazonaws.com/it15116420awabucket/security.mp4",
      content:
        "The election result validity can be confiremed though the security tab"
    }
  ];

  constructor() {}

  ngOnInit() {}
}
