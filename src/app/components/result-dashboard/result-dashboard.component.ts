import { environment } from "./../../../environments/environment.prod";
import { Component, OnInit } from "@angular/core";
import { PalathsabaServiceService } from "../../services/palathsaba/palathsaba-service.service";
import {
  AngularFirestore,
  AngularFirestoreCollection
} from "angularfire2/firestore";
import { AuthServiceService } from "../../services/Auth/auth-service.service";

@Component({
  selector: "app-result-dashboard",
  templateUrl: "./result-dashboard.component.html",
  styleUrls: ["./result-dashboard.component.scss"]
})
export class ResultDashboardComponent implements OnInit {

  URL:any = "";

  constructor(private _PalathsabaServiceService: PalathsabaServiceService,
    private afs: AngularFirestore,
    private _AuthServiceService: AuthServiceService
  ) {}

  electionCollection: AngularFirestoreCollection<any> = this.afs.collection(
    "Election"
  );
  provinceArr = [];
  districtArr = [];
  palathsabaArr = [];
  selectedProvince: any = "all";
  selectedDistrict: any = "all";
  selectedPalathsaba: any = "all";
  privateElectionTypeFlag: boolean = false;
  showLeaderboardFlag: boolean = false;
  ParliamentaryElection: boolean = false;
  buttonBounce: boolean = false;
  periodicalUpdateStatus: boolean = false;
  endDate: any = "";

  ngOnInit() {

    this._AuthServiceService.electionIdCast.subscribe(data => {
      this.URL = `http://localhost:9000/api/v1/distributedpoint/elections/distributedAccessPoint?id=${data}`
    });

    this.electionCollection.valueChanges().subscribe((ElectionData) => {
      // 1: 'Presidential',
      // 2: 'Parliamentary',
      // 3: 'Basic'
      this.ParliamentaryElection = false;
      // ******* get only first object of the election colletion *****

      this._AuthServiceService.electionIdCast.subscribe(data => {
        for (let electionobj of ElectionData) {
          if (electionobj.id == data) {
            if (!this.getPeriodicalUpdateStatus(electionobj)) {
              this.periodicalUpdateStatus = false;
              if (electionobj.category == 3) {
                //private election
                this.privateElectionTypeFlag = false;
                this.filterFromData();
              } else if (
                electionobj.category == 1 ||
                electionobj.category == 2
              ) {
                //public election
                this.privateElectionTypeFlag = true;
                if (electionobj.category == 2) {
                  this.ParliamentaryElection = true;
                }
                this.filterFromData();
              }
            } else {
              this.endDate = this.yyyymmdd(electionobj.end_date);
              this.periodicalUpdateStatus = true;
            }
          }
        }
      });
    });
  }

  getPeriodicalUpdateStatus(electionobj) {
    
    return (
      new Date(electionobj.end_date).getTime() + environment.resultDisplayTime >= new Date().getTime() && electionobj.live_update == 0
    );
  }

  yyyymmdd(date) {
    var x = new Date(date);
    var y = x.getFullYear().toString();
    var m = (x.getMonth() + 1).toString();
    var d = x.getDate().toString();
    d.length == 1 && (d = "0" + d);
    m.length == 1 && (m = "0" + m);
    var yyyymmdd = y +"-"+ m +"-"+ d;
    return yyyymmdd;
  }

  onChangeProvince() {
    this.buttonBounce = true;
    this.filterFromData();
  }

  onChangeDistrict() {
    this.buttonBounce = true;
    this.filterFromData();
  }

  onChangePalathsaba() {
    this.buttonBounce = true;
    this.filterFromData();
  }

  opentab() {
    window.open(
      this.URL,
      '_blank' // <- This is what makes it open in a new window.
    );
  }

  refreshData() {
    this.buttonBounce = false;
    this.selectedProvince = "all";
    this.selectedDistrict = "all";
    this.selectedPalathsaba = "all";
    this._PalathsabaServiceService.setSelectedDistrict(this.selectedDistrict);
    this._PalathsabaServiceService.setSelectedProvince(this.selectedProvince);
    this._PalathsabaServiceService.setSelectedPalathsaba(
      this.selectedPalathsaba
    );

    this._PalathsabaServiceService.filterData.subscribe(data => {
      //console.log(data);
      this.showLeaderboardFlag = true;
      if (this.ParliamentaryElection) {
        this.showLeaderboardFlag = this._PalathsabaServiceService.getProvincialElectionFlag();
      }
      this.provinceArr = data.province;
      this.districtArr = data.district;
      this.palathsabaArr = data.palathsaba;
      // console.log(this.palathsabaArr);
    });
  }

  filterFromData() {
    this._PalathsabaServiceService.setSelectedDistrict(this.selectedDistrict);
    this._PalathsabaServiceService.setSelectedProvince(this.selectedProvince);
    this._PalathsabaServiceService.setSelectedPalathsaba(
      this.selectedPalathsaba
    );

    this._PalathsabaServiceService.filterData.subscribe(data => {
       console.log(data);
      this.showLeaderboardFlag = true;
      if (this.ParliamentaryElection) {
        this.showLeaderboardFlag = this._PalathsabaServiceService.getProvincialElectionFlag();
      }

      this.provinceArr = data.province;
      this.districtArr = data.district;
      this.palathsabaArr = data.palathsaba;
    });
  }
}
