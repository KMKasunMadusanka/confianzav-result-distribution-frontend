import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributedAccessPointComponent } from './distributed-access-point.component';

describe('DistributedAccessPointComponent', () => {
  let component: DistributedAccessPointComponent;
  let fixture: ComponentFixture<DistributedAccessPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributedAccessPointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributedAccessPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
