import { Component, OnInit } from '@angular/core';
import { DistributedAccessPointService } from '../../../services/distributed-access-point/distributed-access-point.service'
import { AuthServiceService } from '../../../services/Auth/auth-service.service'
@Component({
  selector: 'app-distributed-access-point',
  templateUrl: './distributed-access-point.component.html',
  styleUrls: ['./distributed-access-point.component.scss']
})
export class DistributedAccessPointComponent implements OnInit {

  constructor(private _DistributedAccessPointService:DistributedAccessPointService,
              private _AuthServiceService: AuthServiceService) { }
  xmlData = "";
  URL:String = "";
  periodicalUpdateStatus: boolean = false;
  endDate: any = "";
  ngOnInit() {

    this._AuthServiceService.electionIdCast.subscribe(data => {
        this.URL = `http://localhost:9000/api/v1/distributedpoint/elections/distributedAccessPoint?id=${data}`
    });

    this._DistributedAccessPointService.getXML.subscribe((data) => {
      this.xmlData = data;
    })
  }

}
