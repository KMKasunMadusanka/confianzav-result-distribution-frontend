import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../../../services/Auth/auth-service.service'
import { BehaviorSubject, Observable } from 'rxjs'
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  islogged = false;
  electionCollection: AngularFirestoreCollection<any> = this.afs.collection('Election');
  title:String = "";
  constructor(private _AuthServiceService: AuthServiceService, private afs: AngularFirestore, ) {
  }

  ngOnInit() {
    this.electionCollection.valueChanges().subscribe((ElectionData) => {
      this._AuthServiceService.electionIdCast.subscribe(data => {
        for (let electionobj of ElectionData) {
          if (electionobj.id == data) {
            this.title = electionobj.name;
          }
        }
      });
    })

    this._AuthServiceService.logedStatusCast.subscribe((data) => {
      this.islogged = data;
     // console.log(data);
    })



  }

  logOut() {
    this._AuthServiceService.logout();
  }

  setUserType(userType) {
    this._AuthServiceService.onChangeUsertype(userType);
  }
}
