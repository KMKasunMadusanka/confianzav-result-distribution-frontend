import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DigitalSignatureService } from '../../services/digital-signature/digital-signature.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { DomSanitizer } from '../../../../node_modules/@angular/platform-browser';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthServiceService } from '../../services/Auth/auth-service.service'
import { environment } from '../../../environments/environment.prod'

@Component({
  selector: 'app-digital-signature',
  templateUrl: './digital-signature.component.html',
  styleUrls: ['./digital-signature.component.scss']
})
export class DigitalSignatureComponent implements OnInit {

  //firebase ref
  digitalSignature: AngularFirestoreCollection<any> = this.afs.collection('Digital_Signature');
  violations: AngularFirestoreCollection<any> = this.afs.collection('Violation');
  signatureData: any;
  signature: any;
  downloadJsonHref: any;
  publickey: any = {};
  status: any = {};
  periodicalUpdateStatus: boolean = false;
  endDate: any = "";
  violationArray: any = [];
  constructor(private afs: AngularFirestore, private sanitizer: DomSanitizer,
    private _HttpClient: HttpClient,
    private _AuthServiceService: AuthServiceService
  ) { }


  ngOnInit() {
    this.digitalSignature.valueChanges().subscribe((digitalSignatureData) => {
      this._AuthServiceService.electionIdCast.subscribe(data => {
        this.violations.valueChanges().subscribe((violationData) => {
          // console.log(data);
          this.violationArray = [];
          for (let violationObj of violationData) {
            if (violationObj.election_id == data) {
              this.violationArray.push(violationObj);
              console.log(this.violationArray)
            }
          }
          console.log(this.violationArray);
        })
        for (let signatureObj of digitalSignatureData) {
          if (signatureObj.data.candidate_data[0].election_id == data) {
            if (!this.getPeriodicalUpdateStatus(signatureObj.data.election_data)) {
              this.signatureData = signatureObj; //get only first value for now
              let electionData = modifyJson(signatureObj.data, signatureObj.data.election_data.category)
              console.log(signatureObj);
              // set signature
              this.signature = this.signatureData.signature;

              // set data to download
              var theJSON = JSON.stringify(electionData);
              var uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
              this.downloadJsonHref = uri;

              // verify data
              let obj = genaraterequest(electionData, this.signature)
              this._HttpClient.post('http://localhost:9000/api/v1/digitalsignature/signature/veryfy', obj)
                .subscribe((status) => {
                  this.status = status
                })
            } else {
              this.endDate = this.yyyymmdd(signatureObj.data.election_data.end_date);
              this.periodicalUpdateStatus = true;
            }

          }
        }
      });
    });


    this._HttpClient.get('http://localhost:9000/api/v1/digitalsignature/publickey')
      .subscribe((publickeyObj) => {
        this.publickey = publickeyObj
      })
  }

  getPeriodicalUpdateStatus(electionobj) {
    console.log(electionobj.end_date)
    return (
      new Date(electionobj.end_date).getTime() + environment.resultDisplayTime >= new Date().getTime() && electionobj.live_update == 0
    );
  }

  yyyymmdd(date) {
    var x = new Date(date);
    var y = x.getFullYear().toString();
    var m = (x.getMonth() + 1).toString();
    var d = x.getDate().toString();
    d.length == 1 && (d = "0" + d);
    m.length == 1 && (m = "0" + m);
    var yyyymmdd = y + "-" + m + "-" + d;
    return yyyymmdd;
  }

}

const modifyJson = (dataObj, catgory) => {
  let electionobj = dataObj.election_data;
  let candidateArr = dataObj.candidate_data;
  let voteArr:any;
  if (catgory ==1 || catgory == 2){
    voteArr = dataObj.vote_data;
  }

  // sorting keys of json
  const electionDataArr = {}
  Object.keys(electionobj).sort().forEach(function (key) {
    electionDataArr[key] = electionobj[key]
  })

  let candidateDataArr = []
  for (let candidateObj of candidateArr) {
    const orderedElectionData = {}
    Object.keys(candidateObj).sort().forEach(function (key) {
      orderedElectionData[key] = candidateObj[key]
    })
    candidateDataArr.push(orderedElectionData)
  }


  let fianlObj = {}

  if (catgory == 1 || catgory == 2) {
    let voteDataArr = []
    for (let voteObj of voteArr) {
      const orderedElectionData = {}
      Object.keys(voteObj).sort().forEach(function (key) {
        orderedElectionData[key] = voteObj[key]
      })
      voteDataArr.push(orderedElectionData)
    }
    fianlObj = {
      'election_data': electionDataArr,
      'candidate_data': candidateDataArr.sort((a, b) => parseInt(a.candidate_sys_id) - parseInt(b.candidate_sys_id) || parseInt(a.candidate_id) - parseInt(b.candidate_id)),
      'vote_data': voteDataArr.sort((a, b) => parseInt(a.id) - parseInt(b.id) || parseInt(a.candidate_id) - parseInt(b.candidate_id))
    }
  } else if (catgory == 3) {
    fianlObj = {
      'election_data': electionDataArr,
      'candidate_data': candidateDataArr.sort((a, b) => parseInt(a.candidate_sys_id) - parseInt(b.candidate_sys_id) || parseInt(a.candidate_id) - parseInt(b.candidate_id))
    }
  }

  return fianlObj;

}


const genaraterequest = (electionData, signature) => {
  return {
    "signature": signature,
    "data": electionData
  }
}