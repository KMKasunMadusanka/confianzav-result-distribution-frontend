import { Component, OnInit } from '@angular/core';
import { VoteServiceService } from '../../../services/election-result/vote-service.service'
import { PrivateElectionResultService } from '../../../services/private-election-result/private-election-result.service'
import { AuthServiceService } from '../../../services/Auth/auth-service.service'
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { environment } from '../../../../environments/environment.prod'
// declare var $: any;
// const DOCUMENT: InjectionToken<Document>;

@Component({
  selector: 'app-vote-summary-line-chart',
  templateUrl: './vote-summary-line-chart.component.html',
  styleUrls: ['./vote-summary-line-chart.component.scss']
})
export class VoteSummaryLineChartComponent implements OnInit {

  constructor(private _VoteServiceService: VoteServiceService,
    private afs: AngularFirestore,
    private _AuthServiceService:AuthServiceService,
    private _PrivateElectionResultService: PrivateElectionResultService) { }

  partyDataArray = [];
  private_axisX = [];
  private_axixY = [];
  axisX = [];
  axisY = [];
  colorArr = [];
  partyArray = [];
  linechart_option = {}
  electionCollection: AngularFirestoreCollection<any> = this.afs.collection('Election');

  ngOnInit() {

    this.electionCollection.valueChanges().subscribe((ElectionData) => {
      // 1: 'Presidential',
      // 2: 'Parliamentary',
      // 3: 'Basic' 

      // ******* get only first object of the election colletion *****
      this._AuthServiceService.electionIdCast.subscribe(data => {
        for (let electionobj of ElectionData){
          if (electionobj.id == data){
            if (!this.getPeriodicalUpdateStatus(electionobj)) {
              if (electionobj.category == 3) { //private election
                this._PrivateElectionResultService.manupulatePrivateVoteData.subscribe((candidateData) => {
                    this.private_axisX = this.private_genarateXvalues(candidateData);
                    this.private_axixY = this.private_genarateYvalues(candidateData);
                    this.linechart_option = this.manupulateChartData(this.private_axisX, this.private_axixY, this.colorArr);
                    this.partyArray = this.private_genarateCadidateIconList(candidateData);
                });
              }
              else if (electionobj.category == 1 || electionobj.category == 2) {  //public election
                this._VoteServiceService.manipulateVoteData.subscribe((data) => {
                  this.partyDataArray = this.genaratePartyArray(data);
                  this.axisX = this.genarateXvalues(this.partyDataArray);
                  this.axisY = this.genarateYvalues(this.partyDataArray);
                  this.colorArr = this.genarateColorvalues(this.partyDataArray);
                  this.linechart_option = this.manupulateChartData(this.axisX, this.axisY, this.colorArr);
                  this.partyArray = this.genaratePartyIconList(this.partyDataArray);
                });
              }
            }

          }
        }
      });
    });



  }

  getPeriodicalUpdateStatus(electionobj) {
    return (
      new Date(electionobj.end_date).getTime() + environment.resultDisplayTime >= new Date().getTime() && electionobj.live_update == 0
    );
  }

  genaratePartyArray = function (candidateArray) {

    let updatedPartyArray = [];
    for (const candidateObj of candidateArray) {
      let party_vote_Count = 0;
      let name_flag = true;
      for (const partyObject of updatedPartyArray) {

        if (partyObject.party_name == candidateObj.party_name) {
          partyObject.vote_count = partyObject.vote_count + candidateObj.vote_count;
          name_flag = false;
        }
      }

      if (name_flag) {
        party_vote_Count = candidateObj.vote_count;
      }
      const tempObj = {
        "party_name": candidateObj.party_name,
        "vote_count": party_vote_Count,
        "party_color": candidateObj.party_color,
        "party_icon": candidateObj.party_icon
      }
      if (name_flag) {
        updatedPartyArray.push(tempObj);
      }

    }
    return updatedPartyArray;
  }


  genarateXvalues = function (voteArray) {
    let arr = [];
    for (const voteObj of voteArray) {
      arr.push(voteObj.vote_count);
    }
    return arr;
  }

  private_genarateXvalues = function (voteArray) {
    let arr = [];
    for (const voteObj of voteArray) {
      arr.push(voteObj.private_vote_Count);
    }
    return arr;
  }

  genarateYvalues = function (voteArray) {
    let arr = [];
    for (const voteObj of voteArray) {
      arr.push(voteObj.party_name.match(/[A-Z]/g).join(''));
    }
    return arr;
  }

  private_genarateYvalues = function (voteArray) {
    let arr = [];
    for (const voteObj of voteArray) {
      arr.push(voteObj.candidate_name.match(/[A-Z]/g).join(''));
    }
    return arr;
  }

  genarateColorvalues = function (voteArray) {
    let arr = [];
    for (const voteObj of voteArray) {
      arr.push(voteObj.party_color);
    }
    return arr;
  }

  genaratePartyIconList = function (voteArray) {
    let arr = [];
    for (const voteObj of voteArray) {
      let tempObj = {
        "sname": voteObj.party_name.match(/[A-Z]/g).join(''),
        "lname": voteObj.party_name,
        "avatar": voteObj.party_icon
      }
      arr.push(tempObj);
    }
    return arr;
  }

  private_genarateCadidateIconList = function (voteArray) {
   // let imageArray = ['man', 'businessman', 'man-tie', 'man-mostach', 'man-spects', 'man-handy', 'man1', 'man2', 'man3', 'man4', 'man5', 'man6', 'man7'];
    let arr = [];
    for (const voteObj of voteArray) {
    //  var imageName = imageArray[Math.floor(Math.random() * imageArray.length)];
      let tempObj = {
        "sname": voteObj.candidate_name.match(/[A-Z]/g).join(''),
        "lname": voteObj.candidate_name,
        "avatar":  voteObj.candidate_icon // "/assets/images//user-images/" + imageName + ".svg"
      }
      arr.push(tempObj);
    }
    return arr;
  }

  manupulateChartData = function (X, Y, colorArr) {
    return {
      // title: {
      //   show: true,
      //   text: 'Election Results Per Voter',
      //   textStyle: {
      //       color: '#130f3dad',
      //       fontFamily: 'Rajdhani,sans-serif',
      //       fontSize: 25
      //   }
      // },
      //   toolbox: {
      //     show: true,
      //     feature: {
      //         mark: {
      //             show: false
      //         },
      //         dataView: {
      //             show: false,
      //             readOnly: false
      //         },
      //         restore: {
      //             show: false
      //         },
      //         saveAsImage: {
      //             show: true
      //         }
      //     }
      // },
      tooltip: {
        show: true,
        trigger: 'axis',
        formatter: function (params) {
          let relVal = params[0].name;
          relVal = relVal + ': ' + params[0].value;
          return relVal;
        },
        showDelay: 0,
        hideDelay: 50,
        transitionDuration: 0,
        backgroundColor: 'rgba(50,50,50,1)',
        borderColor: '#aaa',
        showContent: true,
        borderRadius: 8,
        padding: 10
      },
      dataZoom: [
        {
          type: 'slider',
          show: false,
          height: 10,
          backgroundColor: '#aaaaaa',
          //         fillerColor: 'rgba(167,183,204,0.4)',
          borderColor: '#0a2b24'
        }
      ],
      axisPointer: {
        type: 'line',
        axis: 'auto'
      },
      legend: {
        show: false,
        data: ['Vote Count']
      },
      xAxis: {
        data: Y
      },
      yAxis: {},
      series: [{
        name: 'Votes',
        type: 'bar',
        barMaxWidth: 60,
        data: X,
        itemStyle: {
          normal: {
            color: function (params) {
              // build a color map as your need.
              const colorList = colorArr
              return colorList[params.dataIndex]
            },
            borderColor: '#f3f3f300',
            borderWidth: 2,
            barBorderRadius: [10, 10, 0, 0],
            //           shadowBlur:10,
            // shadowColor:'rgba(168,225,226,0.5)',
            //           shadowOffsetX:10,
            //           shadowOffsetY:10,
            opacity: .8
          },
          emphasis: {
            //color:'#30336b',
            borderColor: '#30336b',
            borderWidth: 2,
            barBorderRadius: [9, 9, 0, 0],
            shadowBlur: 30,
            shadowColor: 'rgba(32,188 ,157,0.8)',
            //           shadowOffsetX:10,
            //           shadowOffsetY:10,
            opacity: .85,
            label: {
              show: true,
              textStyle: {
                color: 'white',
                fontFamily: 'Rajdhani,sans-serif',
                fontSize: 15
              }
            }
          }
        },
        markPoint: {
          symbol: 'circle',
          symbolSize: 50,
          symbolOffset: [0, 0],
          silent: true
        }
      }],
      label: {
        normal: {
          show: true,
          position: 'top'
        }
        //       emphasis:{
        //         show:false,
        //         position:[10,10],
        //         formatter:'{b}:{c}',
        //         textStyle:{
        //           color:'#fff',
        //           fontWeight:'bolder',
        //           fontSize:14
        //         }
        //       }
      }
    };
  }


  // partyArray = [
  //   {
  //     "sname": "UNP",
  //     "lname": "United National Party",
  //     "avatar": "/assets/images/party-icon/fire.svg"
  //   },
  //   {
  //     "sname": "UPFA",
  //     "lname": "United People's Freedom Alliance",
  //     "avatar": "/assets/images/party-icon/swords.svg"
  //   },
  //   {
  //     "sname": "JVP",
  //     "lname": "Janatha Vimukthi Peramuna",
  //     "avatar": "/assets/images/party-icon/sand-clock.svg"
  //   },
  //   {
  //     "sname": "ITAK",
  //     "lname": "Illankai Tamil Arasu Kadchi",
  //     "avatar": "/assets/images/party-icon/christmas-tree.svg"
  //   }
  // ]








}
