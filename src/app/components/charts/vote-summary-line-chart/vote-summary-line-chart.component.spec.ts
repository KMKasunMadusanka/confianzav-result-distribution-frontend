import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoteSummaryLineChartComponent } from './vote-summary-line-chart.component';

describe('VoteSummaryLineChartComponent', () => {
  let component: VoteSummaryLineChartComponent;
  let fixture: ComponentFixture<VoteSummaryLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoteSummaryLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoteSummaryLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
