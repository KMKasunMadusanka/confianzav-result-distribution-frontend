import { AboutUsComponent } from './../../about-us/about-us.component';
import { Component, OnInit } from '@angular/core';
import { VoteServiceService } from '../../../services/election-result/vote-service.service';
import { PrivateElectionResultService } from '../../../services/private-election-result/private-election-result.service'
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AuthServiceService } from '../../../services/Auth/auth-service.service'
import { environment } from '../../../../environments/environment.prod'
import { TransactionLogService } from '../../../services/transaction-log/transaction-log.service'

@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.scss']
})
export class LeaderBoardComponent implements OnInit {
  electionCollection: AngularFirestoreCollection<any> = this.afs.collection('Election');
  candidateArray = [];
  transactionLogArray:Array<any> = [];

  constructor(private _VoteServiceService: VoteServiceService,
    private afs: AngularFirestore,
    private _AuthServiceService:AuthServiceService,
    private _PrivateElectionResultService: PrivateElectionResultService,
    private _TransactionLogService:TransactionLogService) { }

  ngOnInit() {
    this.electionCollection.valueChanges().subscribe((ElectionData) => {
      // 1: 'Presidential',
      // 2: 'Parliamentary',
      // 3: 'Basic' 

      // ******* get only first object of the election colletion *****
      
      this._AuthServiceService.electionIdCast.subscribe(data => {
        for (let electionobj of ElectionData){
          if (electionobj.id == data){
            if (!this.getPeriodicalUpdateStatus(electionobj)) {
              if (electionobj.category == 3) { //private election
                this._PrivateElectionResultService.manupulatePrivateVoteData.subscribe((candidateData) => {
                  this.candidateArray = this.private_genarateCandidateArray(candidateData);
                });
              }
              else if (electionobj.category == 1 || electionobj.category == 2) {  //public election
                this._VoteServiceService.manipulateVoteData.subscribe((data) => {
                  this.candidateArray = this.genarateCandidateArray(data);
                });
              }
            }  
          }
        }
      });
    });
  }

  getPeriodicalUpdateStatus(electionobj) {
    return (
      new Date(electionobj.end_date).getTime() + environment.resultDisplayTime >= new Date().getTime() && electionobj.live_update == 0
    );
  }

  genarateCandidateArray = function (candidateArray) {
    //let imageArray = ['man', 'businessman', 'man-tie', 'man-mostach', 'man-spects', 'man-handy', 'man1', 'man2', 'man3', 'man4', 'man5', 'man6', 'man7'];
    let updatedCandaidateArray = [];
    for (const candidateObj of candidateArray) {
      //var imageName = imageArray[Math.floor(Math.random() * imageArray.length)];
      const tempObj = {
        "id":candidateObj.candidate_id,
        "name": candidateObj.candidate_name,
        "party": candidateObj.party_name.match(/[A-Z]/g).join(''),
        "vote_count": candidateObj.vote_count,
        "pic":  candidateObj.candidate_icon   // "/assets/images/user-images/" + imageName + ".svg"
      }
      updatedCandaidateArray.push(tempObj);
    }
    return updatedCandaidateArray.sort(function (a, b) { return b.vote_count - a.vote_count });
  }

  private_genarateCandidateArray = function (candidateArray) {
    //let imageArray = ['man', 'businessman', 'man-tie', 'man-mostach', 'man-spects', 'man-handy', 'man1', 'man2', 'man3', 'man4', 'man5', 'man6', 'man7'];
    let updatedCandaidateArray = [];
    for (const candidateObj of candidateArray) {
     // var imageName = imageArray[Math.floor(Math.random() * imageArray.length)];
      const tempObj = {
        "id":candidateObj.candidate_id,
        "name": candidateObj.candidate_name,
        "party": "",
        "vote_count": candidateObj.private_vote_Count,
        "pic": candidateObj.candidate_icon // "/assets/images/user-images/" + imageName + ".svg"
      }
      updatedCandaidateArray.push(tempObj);
    }
    return updatedCandaidateArray.sort(function (a, b) { return b.vote_count - a.vote_count });
  }

  onCanidateClicked = (candidateId) => {
    console.log(candidateId);
    this._TransactionLogService.getCndidateTransactionLog(candidateId).subscribe((data)=>{
      this.transactionLogArray = data.content.transactionLogs;
    });
  }

}
