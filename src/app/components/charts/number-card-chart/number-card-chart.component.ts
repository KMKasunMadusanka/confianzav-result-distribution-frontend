import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-number-card-chart',
  templateUrl: './number-card-chart.component.html',
  styleUrls: ['./number-card-chart.component.scss']
})
export class NumberCardChartComponent implements OnInit {

  constructor() { }

  number_option = {
    // title: {
    //     text: 'Election Results',
    //     left: 'left',
    //     textStyle: {
    //       color: '#130f3dad',
    //       fontFamily: 'Rajdhani,sans-serif',
    //       fontSize: 22
    //     }
    // },
    tooltip: {
        trigger: 'item',
        formatter: '{b}'
    },
    // toolbox: {
    //     show: true,
    //     feature: {
    //         mark: {
    //             show: false
    //         },
    //         dataView: {
    //             show: false,
    //             readOnly: false
    //         },
    //         restore: {
    //             show: true
    //         },
    //         saveAsImage: {
    //             show: true
    //         }
    //     }
    // },
    series: [{
        type: 'treemap',
        left: 'center',
        width: '100%',
        height: '100%',
        itemStyle: {
            normal: {
                label: {
                    show: true,
                    formatter: '{b}'
                },
                borderWidth: 2
            },
            emphasis: {
                label: {
                    show: true
                }
            }
        },
        label: {
            normal: {
                fontSize: 25,
                fontFamily: 'Rajdhani,sans-serif',
            }
        },
        roam: false,
        nodeClick: false,
        data: [{
                name: 'UNP\n397.38',
                value: 2000.38
            }, {
                name: 'JVP\n\n3584.6',
                value: 3584.6
            }, {
                name: 'NPF\n\n7138.52',
                value: 7138.52
            }, {
                name: 'HHJK\n\n2389.25',
                value: 2389.25
            }
        ]
    }],
    color: ['#20bf6b', '#44bd32', '#fc5c65', '#4b6584']
  };

  ngOnInit() {
  }

}
