import { Component, OnInit } from '@angular/core';
import { VoteServiceService } from '../../../services/election-result/vote-service.service';
@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

  constructor(private _VoteServiceService: VoteServiceService) { }

  ngOnInit() {

  }

}
