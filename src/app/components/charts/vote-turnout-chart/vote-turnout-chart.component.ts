import { Component, OnInit } from '@angular/core';
import { NgxEchartsService } from 'ngx-echarts';
import { VoteServiceService } from '../../../services/election-result/vote-service.service'
import { PrivateElectionResultService } from '../../../services/private-election-result/private-election-result.service'
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AuthServiceService } from '../../../services/Auth/auth-service.service'
import {  environment  } from '../../../../environments/environment.prod'
@Component({
    selector: 'app-vote-turnout-chart',
    templateUrl: './vote-turnout-chart.component.html',
    styleUrls: ['./vote-turnout-chart.component.scss']
})
export class VoteTurnoutChartComponent implements OnInit {

    voteCount = 0;
    option = {}
    max_limit = 0;
    electionCollection: AngularFirestoreCollection<any> = this.afs.collection('Election');

    constructor(private _VoteServiceService: VoteServiceService,
        private afs: AngularFirestore,
        private _AuthServiceService: AuthServiceService,
        private _PrivateElectionResultService: PrivateElectionResultService) { }

    ngOnInit() {

        this.electionCollection.valueChanges().subscribe((ElectionData) => {
            // 1: 'Presidential',
            // 2: 'Parliamentary',
            // 3: 'Basic' 

            // ******* get only first object of the election colletion *****

            this._AuthServiceService.electionIdCast.subscribe(data => {
                for (let electionobj of ElectionData) {
                    if (electionobj.id == data) {
                        if (!this.getPeriodicalUpdateStatus(electionobj)) {
                            if (electionobj.category == 3) { //private election
                                this._PrivateElectionResultService.manupulatePrivateVoteData.subscribe((candidateData) => {
                                    this.voteCount = this.private_calculateVoterTurnout(candidateData);
                                    this.max_limit = electionobj.expected_vote_count;
                                    this.option = this.geanrateChart(this.voteCount, this.max_limit);
                                });
                            }
                            else if (electionobj.category == 1 || electionobj.category == 2) {  //public election
                                this._VoteServiceService.manipulateVoteData.subscribe((data) => {
                                    this.voteCount = this.calculateVoterTurnout(data);
                                    this.max_limit = this.calculateEligibleVotes(data);
                                    this.option = this.geanrateChart(this.voteCount, this.max_limit);
                                });
                            }
                        }  
                    }
                }
            });
        });



    }

    getPeriodicalUpdateStatus(electionobj) {
        return (
            new Date(electionobj.end_date).getTime() + environment.resultDisplayTime >= new Date().getTime() && electionobj.live_update == 0
        );
      }

    calculateVoterTurnout = function (voteArray) {
        let voteCount = 0;
        for (const candidateObj of voteArray) {
            voteCount = voteCount + candidateObj.vote_count;
        }
        return voteCount;
    }

    private_calculateVoterTurnout = function (voteArray) {
        let voteCount = 0;
        for (const candidateObj of voteArray) {
            voteCount = voteCount + candidateObj.private_vote_Count;
        }
        return voteCount;
    }

    calculateEligibleVotes = function (voteArray) {
        let voteCount = 0;
        for (const candidateObj of voteArray) {
            const elegibleArray = candidateObj.elegible_vote_count_obj
            for (const plathsabaObj of elegibleArray) {
                voteCount = voteCount + plathsabaObj.eligible_vote;
            }
        }

        return voteCount;
    }

    geanrateChart = function (voteCount, maxlimit) {

        let presentage = Math.round((voteCount / maxlimit) * 100 * 100) / 100;


        return {
            tooltip: {
                formatter: "{a} <br/>{c} {b}"
            },
            // title: {
            //     text: 'Vote Turnout',
            //     left: 'center',
            //     textStyle: {
            //         color: '#130f3dad',
            //         fontFamily: 'Rajdhani,sans-serif',
            //         fontSize: 22
            //     }
            // },
            backgroundColor: 'rgba(255,255,255,0)',
            series: [
                {
                    name: 'vote Turnout',
                    type: 'gauge',
                    radius: '90%',
                    min: 0,
                    max: 100,
                    startAngle: 200,
                    endAngle: -20,
                    splitNumber: 5,
                    axisLine: {            // 坐标轴线
                        lineStyle: {       // 属性lineStyle控制线条样式
                            width: 8
                        }
                    },
                    axisTick: {            // 坐标轴小标记
                        splitNumber: 5,
                        length: 10,        // 属性length控制线长
                        lineStyle: {       // 属性lineStyle控制线条样式
                            color: 'auto'
                        }
                    },
                    axisLabel: {
                        formatter: function (v) {
                            switch (v + '') {
                                case '0': return '1';
                                case '1': return '';
                                case '100': return maxlimit.toString();
                            }
                        },
                        textStyle: {
                            color: 'black',
                            fontFamily: 'Rajdhani,sans-serif',
                            fontSize: 15,
                            fontWeight: 900
                        }
                    },
                    splitLine: {           // 分隔线
                        length: 15,         // 属性length控制线长
                        lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
                            color: 'auto'
                        }
                    },
                    pointer: {
                        width: 5
                    },
                    title: {
                        show: true
                    },
                    detail: {
                        show: true,
                        formatter: '{value}%',
                        textStyle: {
                            color: 'black',
                            fontFamily: 'Rajdhani,sans-serif',
                            fontSize: 40,
                            fontWeight: 900
                        }
                    },
                    data: [{
                        value: presentage,
                        // name: '200',
                    }]
                }
            ]
        };
    }




}
