import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoteTurnoutChartComponent } from './vote-turnout-chart.component';

describe('VoteTurnoutChartComponent', () => {
  let component: VoteTurnoutChartComponent;
  let fixture: ComponentFixture<VoteTurnoutChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoteTurnoutChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoteTurnoutChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
