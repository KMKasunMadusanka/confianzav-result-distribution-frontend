import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-vote-presentage-pie-chart',
    templateUrl: './vote-presentage-pie-chart.component.html',
    styleUrls: ['./vote-presentage-pie-chart.component.scss']
})
export class VotePresentagePieChartComponent implements OnInit {

    constructor() { }



    option_pie_chart = {
        // title: {
        //     text: 'Party Wise Election Resuly',
        //     x: 'center',
        //     textStyle: {
        //         color: '#130f3dad',
        //         fontFamily: 'Rajdhani,sans-serif',
        //         fontSize: 22
        //     }
        // },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: ['UNP', 'UPFA', 'JVP', 'NNP', 'SLPD']
        },
        series: [
            {
                name: 'Vote Count',
                type: 'pie',
                radius: '70%',
                center: ['50%', '60%'],
                data: [
                    { value: 335, name: 'UNP' },
                    { value: 310, name: 'UPFA' },
                    { value: 234, name: 'JVP' },
                    { value: 135, name: 'NNP' },
                    { value: 1548, name: 'SLPD' }
                ],
                color: ['green', 'blue', 'red', 'yellow','gray'],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };

    ngOnInit() {
    }

}
