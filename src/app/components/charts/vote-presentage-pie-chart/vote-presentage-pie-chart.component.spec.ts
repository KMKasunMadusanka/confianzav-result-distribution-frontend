import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VotePresentagePieChartComponent } from './vote-presentage-pie-chart.component';

describe('VotePresentagePieChartComponent', () => {
  let component: VotePresentagePieChartComponent;
  let fixture: ComponentFixture<VotePresentagePieChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VotePresentagePieChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotePresentagePieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
