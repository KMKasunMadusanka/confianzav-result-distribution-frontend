import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ResultDashboardComponent } from './components/result-dashboard/result-dashboard.component';
import { DigitalSignatureComponent } from './components/digital-signature/digital-signature.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { DataTableComponent } from './components/charts/data-table/data-table.component';
import { VoteTurnoutChartComponent } from './components/charts/vote-turnout-chart/vote-turnout-chart.component';
import { VotePresentagePieChartComponent } from './components/charts/vote-presentage-pie-chart/vote-presentage-pie-chart.component';
import { VoteSummaryLineChartComponent } from './components/charts/vote-summary-line-chart/vote-summary-line-chart.component';
import { NumberCardChartComponent } from './components/charts/number-card-chart/number-card-chart.component';
import { environment } from '../environments/environment';
import { VoteServiceService } from './services/election-result/vote-service.service';
import { PalathsabaServiceService } from './services/palathsaba/palathsaba-service.service';
import { PrivateElectionResultService } from './services/private-election-result/private-election-result.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthServiceService } from './services/Auth/auth-service.service';
import { AuthGuard } from './auth.guard';
import { LeaderBoardComponent } from './components/charts/leader-board/leader-board.component';
import { LoginComponent } from './components/login/login.component';
import { ParticlesModule } from 'angular-particle';
import { NavBarComponent } from './components/nav-bar/nav-bar/nav-bar.component';
import { DistributedAccessPointComponent } from './components/distributed-access-point/distributed-access-point/distributed-access-point.component';
import { TransactionLogService } from './services/transaction-log/transaction-log.service'

import {MatButtonModule,
        MatCheckboxModule,
        MatToolbarModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatSelectModule,
} from '@angular/material';






const routes: Routes = [
  { path: 'dashboard', component: ResultDashboardComponent, canActivate: [AuthGuard]},
  { path: 'digital-signature', component: DigitalSignatureComponent, canActivate: [AuthGuard]},
  { path: 'distributed-access-point', component: DistributedAccessPointComponent},
  { path: 'aboutus', component: AboutUsComponent},
  { path: 'login', component: LoginComponent},
  { path: '', redirectTo:'/aboutus',pathMatch:'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    ResultDashboardComponent,
    DigitalSignatureComponent,
    AboutUsComponent,
    DataTableComponent,
    VoteTurnoutChartComponent,
    VotePresentagePieChartComponent,
    VoteSummaryLineChartComponent,
    NumberCardChartComponent,
    LeaderBoardComponent,
    LoginComponent,
    NavBarComponent,
    DistributedAccessPointComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    SweetAlert2Module.forRoot(),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSidenavModule,
    FormsModule,
    MatIconModule,
    MatInputModule,
    NgxEchartsModule,
    MatListModule,
    MatSelectModule,
    HttpClientModule,
    ReactiveFormsModule,
    ParticlesModule
  ],
  providers: [
    VoteServiceService,
    PalathsabaServiceService,
    PrivateElectionResultService,
    AuthServiceService,
    AuthGuard,
    TransactionLogService
  ],
  exports: [ RouterModule ],
  bootstrap: [AppComponent],
})
export class AppModule { }
